//
//  RatesTests.swift
//  ConversionsTests
//
//  Created by Daniel Griso Filho on 10/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import XCTest
@testable import Conversions

class RatesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testsRates() {
        let rates = Rates(object: MockJson.rates)
        
        XCTAssertEqual(rates.aUD, 1.6106)
        XCTAssertEqual(rates.bGN,1.9488)
        XCTAssertEqual(rates.bRL,4.7747)
        XCTAssertEqual(rates.cAD,1.5283)
        XCTAssertEqual(rates.cHF,1.1235)
        XCTAssertEqual(rates.cNY,7.9168)
        XCTAssertEqual(rates.cZK,25.623)
        XCTAssertEqual(rates.dKK,7.4301)
        XCTAssertEqual(rates.gBP,0.89504)
        XCTAssertEqual(rates.hKD,9.0998)
        XCTAssertEqual(rates.hRK,7.4076)
        XCTAssertEqual(rates.hUF,325.33)
        XCTAssertEqual(rates.iDR,17262)
        XCTAssertEqual(rates.iLS,4.1557)
        XCTAssertEqual(rates.iNR,83.419)
        XCTAssertEqual(rates.iSK,127.34)
        XCTAssertEqual(rates.jPY,129.09)
        XCTAssertEqual(rates.kRW,1300.1)
        XCTAssertEqual(rates.mXN,22.286)
        XCTAssertEqual(rates.mYR,4.7948)
        XCTAssertEqual(rates.nOK,9.7412)
        XCTAssertEqual(rates.nZD,1.757)
        XCTAssertEqual(rates.pHP,62.369)
        XCTAssertEqual(rates.pLN,4.3029)
        XCTAssertEqual(rates.rON,4.622)
        XCTAssertEqual(rates.rUB,79.291)
        XCTAssertEqual(rates.sEK,10.553)
        XCTAssertEqual(rates.sGD,1.5943)
        XCTAssertEqual(rates.tHB,37.994)
        XCTAssertEqual(rates.tRY,7.601)
        XCTAssertEqual(rates.uSD,1.1593)
        XCTAssertEqual(rates.zAR,17.76)
    }
}
