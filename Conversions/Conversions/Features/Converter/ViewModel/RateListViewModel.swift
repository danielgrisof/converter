//
//  RateListViewModel.swift
//  Conversions
//
//  Created by Daniel Griso Filho on 08/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import Foundation

class RateListViewModel {

    // MARK: - Properties

    fileprivate var itemsViewModels = [IndexPath: RateViewModel]()

    var numberOfItems: Int { return itemsViewModels.count }
    
    // MARK: - Initialization
    
    convenience init(_ objects: [String: Any]) {
        self.init()
        self.itemsViewModels = objects.enumerated().reduce(into: [IndexPath: RateViewModel]()) {
            let newIndexPath = IndexPath(row: $1.offset, section: 0)
            $0[newIndexPath] = RateViewModel($1.element.key, $1.element.value)
        }
    }

    // MARK: - View Model
    
    func itemViewModel(indexPath: IndexPath) -> RateViewModel? {
        if itemsViewModels.keys.contains(indexPath) {
            return itemsViewModels[indexPath]
        }
        return nil
    }

}
