//
//  ConverterView.swift
//  Converter
//
//  Created by Daniel Griso Filho on 05/02/19.
//Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import UIKit
import FlagKit

//MARK: ConverterView Class
final class ConverterView: TableUserInterface {
    
    // MARK: - Variables
    var viewModel = BaseListViewModel()
    var ratesViewModel = RateListViewModel()
    var timer = Timer()
    
    // MARK: - Outlets
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var currencyInputedTextField: UITextField!
    
    // MARK: - Life Cicle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = AppModules.converter.viewName.uppercasedFirst
        updateRate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTableView()
        presenter.getRates(country: ServiceConstants.UrlParts.baseCountry.rawValue)
        view.addToolBar(textField: currencyInputedTextField)
    }
    
    // MARK: - Intenal Functions
    fileprivate func setupTableView() {
        tableView.registerNibCell(ConverterCell.self)
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
    }
    
    @objc fileprivate func requestCurrency() {
        if let country = viewModel.itemViewModel(indexPath: IndexPath(row: 0, section: 0))?.base {
            presenter.getRates(country: country)
        }
    }
    
    fileprivate func updateRate() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(requestCurrency), userInfo: nil, repeats: true)
    }
    
    // MARK: - Actions
}

//MARK: - ConverterView API
extension ConverterView: ConverterViewApi {
    func updateRates(viewModel: BaseListViewModel, rates: RateListViewModel) {
        self.viewModel = viewModel
        self.ratesViewModel = rates
        tableView.reloadData()
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController.showSimpleAlert(message: message)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - ConverterView UITableView API
extension ConverterView {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ratesViewModel.numberOfItems
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let name = viewModel.itemViewModel(indexPath: IndexPath(row: 0, section: section))?.base
        countryNameLabel.text = name ?? ""
        let shortCountryName = name?.dropLast()
        let flag = Flag(countryCode: String(shortCountryName ?? ""))
        flagImageView?.image = flag?.originalImage
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ConverterCell.cellReuseId(), for: indexPath)  as? ConverterCell else { return UITableViewCell() }
        
        cell.viewModel = ratesViewModel.itemViewModel(indexPath: indexPath)
        cell.newValue = currencyInputedTextField.text?.floatValue
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        timer.invalidate()
        currencyInputedTextField.text = ""
        if let vm = ratesViewModel.itemViewModel(indexPath: indexPath) {
            presenter.getRates(country: vm.name)
            updateRate()
        }else {
            showAlert(message: HTTPStatus.Fail.notFound.description)
        }
    }
}

// MARK: - ConverterView Viper Components API
private extension ConverterView {
    var presenter: ConverterPresenterApi {
        return _presenter as! ConverterPresenterApi
    }

}
