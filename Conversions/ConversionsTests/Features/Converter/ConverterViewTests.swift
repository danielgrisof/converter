//
//  ConverterViewTests.swift
//  ConversionsTests
//
//  Created by Daniel Griso Filho on 10/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import XCTest
@testable import Conversions

class ConverterViewTests: XCTestCase {

    class converterViewApiMock: ConverterViewApi {
        var _presenter: Presenter!
        var _displayData: DisplayData!
        
        func updateRates(viewModel: BaseListViewModel, rates: RateListViewModel) {
            XCTAssertNotNil(viewModel)
            XCTAssertNotNil(rates)
        }
        
        func showAlert(message: String) {
            XCTAssertNotNil(message)
        }
    }
    
    let viewMock = converterViewApiMock()
    let payload = Payload(object: MockJson.payload)
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testUpdateRates() {
        let base = BaseListViewModel([payload])
        let rates = RateListViewModel(MockJson.rates)
        viewMock.updateRates(viewModel: base, rates: rates)
    }
    
    func testShowAlert() {
        viewMock.showAlert(message: "Alert Mock")
    }
    
    func testPresenterReferenceFail() {
        XCTAssertNil(viewMock._presenter)
    }
    
    func testDisplayDataReferenceFail() {
        XCTAssertNil(viewMock._displayData)
    }

}
