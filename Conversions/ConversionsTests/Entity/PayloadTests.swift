//
//  PayloadTests.swift
//  ConversionsTests
//
//  Created by Daniel Griso Filho on 10/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import XCTest
@testable import Conversions

class PayloadTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testsPayload() {
        let payload = Payload(object: MockJson.payload)
        
        XCTAssertEqual(payload.base, "EUR")
        XCTAssertEqual(payload.date, "2018-09-06")
        XCTAssertEqual(payload.rates?.aUD, 1.6106)
    }
}
