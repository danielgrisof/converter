//
//  ConverterRouterTests.swift
//  ConversionsTests
//
//  Created by Daniel Griso Filho on 10/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import XCTest
@testable import Conversions

class ConverterRouterTests: XCTestCase {
    
    class converterRouterApiMock: ConverterRouterApi {
        var _presenter: Presenter!
        
        var _view: UIViewController!
        
        func show(inWindow window: UIWindow?, embedInNavController: Bool, setupData: Any?, makeKeyAndVisible: Bool) {
            XCTAssertNotNil(window)
        }
        
        func show(from: Router, embedInNavController: Bool, setupData: Any?, parentRouter: Router?) {
            XCTAssertNotNil(from)
        }
        
        func show(from containerView: UIViewController, insideView targetView: UIView, setupData: Any?) {
            XCTAssertNotNil(containerView)
        }
        
        func present(from: UIViewController, embedInNavController: Bool, presentationStyle: UIModalPresentationStyle, transitionStyle: UIModalTransitionStyle, setupData: Any?, completion: (() -> Void)?) {
            XCTAssertNotNil(from)
        }
        
        func needsUpdate() {}
        
        func dismiss(parentNeedsUpdate: Bool) {
            XCTAssertNotNil(parentNeedsUpdate)
        }
        
        func pop(parentNeedsUpdate: Bool) {
            XCTAssertNotNil(parentNeedsUpdate)
        }
        
        
    }

    let routerMock = converterRouterApiMock()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testShowWindows() {
        routerMock.show(inWindow: UIWindow(), embedInNavController: true, setupData: nil, makeKeyAndVisible: true)
    }
    
    func testShowFromRouter() {
        routerMock.show(from: Router(), embedInNavController: true, setupData: nil, parentRouter: nil)
    }
    
    func testShowContainerView() {
        routerMock.show(from: UIViewController(), insideView: UIView(), setupData: nil)
    }
    
    func testPresent() {
        routerMock.present(from: UIViewController(), embedInNavController: true, presentationStyle: .custom, transitionStyle: .coverVertical, setupData: nil, completion: nil)
    }
    
    func testDismiss() {
        routerMock.dismiss(parentNeedsUpdate: true)
    }
    
    func testPop() {
        routerMock.pop(parentNeedsUpdate: true)
    }
    
}
