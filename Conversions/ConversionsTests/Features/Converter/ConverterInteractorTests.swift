//
//  ConverterInteractorTests.swift
//  ConversionsTests
//
//  Created by Daniel Griso Filho on 10/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import XCTest
@testable import Conversions

class ConverterInteractorTests: XCTestCase {

    class converterInteractorApiMock: ConverterInteractorApi {
        var _presenter: Presenter!
        func callAPI(country: String) {
            XCTAssertNotNil(country)
        }
    }
    
    let interactorMock = converterInteractorApiMock()
    let payload = Payload(object: MockJson.payload)
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCallAPI() {
        XCTAssertNotNil(payload.base)
        interactorMock.callAPI(country: payload.base ?? "")
    }
    
    func testPresenterReferenceFail() {
        XCTAssertNil(interactorMock._presenter)
    }
    
}
