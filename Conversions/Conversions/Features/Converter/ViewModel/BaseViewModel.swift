//
//  BaseViewModel.swift
//  Conversions
//
//  Created by Daniel Griso Filho on 08/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import Foundation

class BaseViewModel {

    // MARK: - Initialization

    convenience init(_ object: Payload?) {
        self.init()
        self.object = object
    }

    // MARK: - Properties

   var object: Payload?
    
    var base: String { return object?.base ?? "" }
    

}
