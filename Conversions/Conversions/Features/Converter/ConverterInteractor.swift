//
//  ConverterInteractor.swift
//  Converter
//
//  Created by Daniel Griso Filho on 05/02/19.
//Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import Foundation

// MARK: - ConverterInteractor Class
final class ConverterInteractor: Interactor {

}

// MARK: - ConverterInteractor API
extension ConverterInteractor: ConverterInteractorApi {
    func callAPI(country: String) {
        
        RemoteDataManager.getApiInformations(endPoint: country) { (payload, status)  in
            guard let payload = payload, let rates = payload.rates?.dictionaryRepresentation() else { self.presenter.didFailRequest(message: status.message); return }
            status.success ? self.presenter.didSuccessfullyRequest(viewModel: BaseListViewModel([payload]),rates: RateListViewModel(rates)) : self.presenter.didFailRequest(message: status.message)
        }
        
    }
}

// MARK: - Interactor Viper Components Api
private extension ConverterInteractor {
    var presenter: ConverterPresenterApi {
        return _presenter as! ConverterPresenterApi
    }
}
