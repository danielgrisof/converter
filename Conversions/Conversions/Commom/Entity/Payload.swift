//
//  Payload.swift
//
//  Created by Daniel Griso Filho on 07/02/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Payload: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let base = "base"
    static let date = "date"
    static let rates = "rates"
  }

  // MARK: Properties
  public var base: String?
  public var date: String?
  public var rates: Rates?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    base = json[SerializationKeys.base].string
    date = json[SerializationKeys.date].string
    rates = Rates(json: json[SerializationKeys.rates])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = base { dictionary[SerializationKeys.base] = value }
    if let value = date { dictionary[SerializationKeys.date] = value }
    if let value = rates { dictionary[SerializationKeys.rates] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.base = aDecoder.decodeObject(forKey: SerializationKeys.base) as? String
    self.date = aDecoder.decodeObject(forKey: SerializationKeys.date) as? String
    self.rates = aDecoder.decodeObject(forKey: SerializationKeys.rates) as? Rates
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(base, forKey: SerializationKeys.base)
    aCoder.encode(date, forKey: SerializationKeys.date)
    aCoder.encode(rates, forKey: SerializationKeys.rates)
  }

}
