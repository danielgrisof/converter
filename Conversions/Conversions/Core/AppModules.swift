//
//  AppModule.swift
//  Viperit
//
//  Created by Ferran on 11/09/2016.
//  Copyright © 2016 Ferran Abelló. All rights reserved.
//

import Foundation

//MARK: - Application modules
public enum AppModules: String, ViperitModule {
    //TODO: - Insert the views here like this
    case converter
}
