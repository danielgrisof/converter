//
//  ServiceConstants.swift
//  A Lodjinha
//
//  Created by Daniel Griso Filho on 1/12/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import UIKit

struct ServiceConstants {
    
    enum UrlParts: String {
        case baseURL = "https://revolut.duckdns.org/latest?base="
        case baseCountry = "EUR"
        case country = "{country}"
    }
    
    struct UrlParam {
        
        struct Key {
            static let country = "{country}"
        }
    }
}
