//
//  Rates.swift
//
//  Created by Daniel Griso Filho on 07/02/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Rates: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let rON = "RON"
        static let mYR = "MYR"
        static let iSK = "ISK"
        static let cAD = "CAD"
        static let dKK = "DKK"
        static let gBP = "GBP"
        static let pHP = "PHP"
        static let cZK = "CZK"
        static let pLN = "PLN"
        static let rUB = "RUB"
        static let sGD = "SGD"
        static let bRL = "BRL"
        static let jPY = "JPY"
        static let sEK = "SEK"
        static let uSD = "USD"
        static let hRK = "HRK"
        static let nZD = "NZD"
        static let hKD = "HKD"
        static let bGN = "BGN"
        static let tRY = "TRY"
        static let mXN = "MXN"
        static let hUF = "HUF"
        static let kRW = "KRW"
        static let nOK = "NOK"
        static let iNR = "INR"
        static let iLS = "ILS"
        static let iDR = "IDR"
        static let cHF = "CHF"
        static let tHB = "THB"
        static let cNY = "CNY"
        static let zAR = "ZAR"
        static let aUD = "AUD"
  }

    // MARK: Properties
    public var rON: Float?
    public var mYR: Float?
    public var iSK: Float?
    public var cAD: Float?
    public var dKK: Float?
    public var gBP: Float?
    public var pHP: Float?
    public var cZK: Float?
    public var pLN: Float?
    public var rUB: Float?
    public var sGD: Float?
    public var bRL: Float?
    public var jPY: Float?
    public var sEK: Float?
    public var uSD: Float?
    public var hRK: Float?
    public var nZD: Float?
    public var hKD: Float?
    public var bGN: Float?
    public var tRY: Float?
    public var mXN: Float?
    public var hUF: Float?
    public var kRW: Float?
    public var nOK: Float?
    public var iNR: Float?
    public var iLS: Float?
    public var iDR: Float?
    public var cHF: Float?
    public var tHB: Float?
    public var cNY: Float?
    public var zAR: Float?
    public var aUD: Float?

    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }

    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        rON = json[SerializationKeys.rON].float
        mYR = json[SerializationKeys.mYR].float
        iSK = json[SerializationKeys.iSK].float
        cAD = json[SerializationKeys.cAD].float
        dKK = json[SerializationKeys.dKK].float
        gBP = json[SerializationKeys.gBP].float
        pHP = json[SerializationKeys.pHP].float
        cZK = json[SerializationKeys.cZK].float
        pLN = json[SerializationKeys.pLN].float
        rUB = json[SerializationKeys.rUB].float
        sGD = json[SerializationKeys.sGD].float
        bRL = json[SerializationKeys.bRL].float
        jPY = json[SerializationKeys.jPY].float
        sEK = json[SerializationKeys.sEK].float
        uSD = json[SerializationKeys.uSD].float
        hRK = json[SerializationKeys.hRK].float
        nZD = json[SerializationKeys.nZD].float
        hKD = json[SerializationKeys.hKD].float
        bGN = json[SerializationKeys.bGN].float
        tRY = json[SerializationKeys.tRY].float
        mXN = json[SerializationKeys.mXN].float
        hUF = json[SerializationKeys.hUF].float
        kRW = json[SerializationKeys.kRW].float
        nOK = json[SerializationKeys.nOK].float
        iNR = json[SerializationKeys.iNR].float
        iLS = json[SerializationKeys.iLS].float
        iDR = json[SerializationKeys.iDR].float
        cHF = json[SerializationKeys.cHF].float
        tHB = json[SerializationKeys.tHB].float
        cNY = json[SerializationKeys.cNY].float
        zAR = json[SerializationKeys.zAR].float
        aUD = json[SerializationKeys.aUD].float
    }

    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = rON { dictionary[SerializationKeys.rON] = value }
        if let value = mYR { dictionary[SerializationKeys.mYR] = value }
        if let value = iSK { dictionary[SerializationKeys.iSK] = value }
        if let value = cAD { dictionary[SerializationKeys.cAD] = value }
        if let value = dKK { dictionary[SerializationKeys.dKK] = value }
        if let value = gBP { dictionary[SerializationKeys.gBP] = value }
        if let value = pHP { dictionary[SerializationKeys.pHP] = value }
        if let value = cZK { dictionary[SerializationKeys.cZK] = value }
        if let value = pLN { dictionary[SerializationKeys.pLN] = value }
        if let value = rUB { dictionary[SerializationKeys.rUB] = value }
        if let value = sGD { dictionary[SerializationKeys.sGD] = value }
        if let value = bRL { dictionary[SerializationKeys.bRL] = value }
        if let value = jPY { dictionary[SerializationKeys.jPY] = value }
        if let value = sEK { dictionary[SerializationKeys.sEK] = value }
        if let value = uSD { dictionary[SerializationKeys.uSD] = value }
        if let value = hRK { dictionary[SerializationKeys.hRK] = value }
        if let value = nZD { dictionary[SerializationKeys.nZD] = value }
        if let value = hKD { dictionary[SerializationKeys.hKD] = value }
        if let value = bGN { dictionary[SerializationKeys.bGN] = value }
        if let value = tRY { dictionary[SerializationKeys.tRY] = value }
        if let value = mXN { dictionary[SerializationKeys.mXN] = value }
        if let value = hUF { dictionary[SerializationKeys.hUF] = value }
        if let value = kRW { dictionary[SerializationKeys.kRW] = value }
        if let value = nOK { dictionary[SerializationKeys.nOK] = value }
        if let value = iNR { dictionary[SerializationKeys.iNR] = value }
        if let value = iLS { dictionary[SerializationKeys.iLS] = value }
        if let value = iDR { dictionary[SerializationKeys.iDR] = value }
        if let value = cHF { dictionary[SerializationKeys.cHF] = value }
        if let value = tHB { dictionary[SerializationKeys.tHB] = value }
        if let value = cNY { dictionary[SerializationKeys.cNY] = value }
        if let value = zAR { dictionary[SerializationKeys.zAR] = value }
        if let value = aUD { dictionary[SerializationKeys.aUD] = value }
        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.rON = aDecoder.decodeObject(forKey: SerializationKeys.rON) as? Float
        self.mYR = aDecoder.decodeObject(forKey: SerializationKeys.mYR) as? Float
        self.iSK = aDecoder.decodeObject(forKey: SerializationKeys.iSK) as? Float
        self.cAD = aDecoder.decodeObject(forKey: SerializationKeys.cAD) as? Float
        self.dKK = aDecoder.decodeObject(forKey: SerializationKeys.dKK) as? Float
        self.gBP = aDecoder.decodeObject(forKey: SerializationKeys.gBP) as? Float
        self.pHP = aDecoder.decodeObject(forKey: SerializationKeys.pHP) as? Float
        self.cZK = aDecoder.decodeObject(forKey: SerializationKeys.cZK) as? Float
        self.pLN = aDecoder.decodeObject(forKey: SerializationKeys.pLN) as? Float
        self.rUB = aDecoder.decodeObject(forKey: SerializationKeys.rUB) as? Float
        self.sGD = aDecoder.decodeObject(forKey: SerializationKeys.sGD) as? Float
        self.bRL = aDecoder.decodeObject(forKey: SerializationKeys.bRL) as? Float
        self.jPY = aDecoder.decodeObject(forKey: SerializationKeys.jPY) as? Float
        self.sEK = aDecoder.decodeObject(forKey: SerializationKeys.sEK) as? Float
        self.uSD = aDecoder.decodeObject(forKey: SerializationKeys.uSD) as? Float
        self.hRK = aDecoder.decodeObject(forKey: SerializationKeys.hRK) as? Float
        self.nZD = aDecoder.decodeObject(forKey: SerializationKeys.nZD) as? Float
        self.hKD = aDecoder.decodeObject(forKey: SerializationKeys.hKD) as? Float
        self.bGN = aDecoder.decodeObject(forKey: SerializationKeys.bGN) as? Float
        self.tRY = aDecoder.decodeObject(forKey: SerializationKeys.tRY) as? Float
        self.mXN = aDecoder.decodeObject(forKey: SerializationKeys.mXN) as? Float
        self.hUF = aDecoder.decodeObject(forKey: SerializationKeys.hUF) as? Float
        self.kRW = aDecoder.decodeObject(forKey: SerializationKeys.kRW) as? Float
        self.nOK = aDecoder.decodeObject(forKey: SerializationKeys.nOK) as? Float
        self.iNR = aDecoder.decodeObject(forKey: SerializationKeys.iNR) as? Float
        self.iLS = aDecoder.decodeObject(forKey: SerializationKeys.iLS) as? Float
        self.iDR = aDecoder.decodeObject(forKey: SerializationKeys.iDR) as? Float
        self.cHF = aDecoder.decodeObject(forKey: SerializationKeys.cHF) as? Float
        self.tHB = aDecoder.decodeObject(forKey: SerializationKeys.tHB) as? Float
        self.cNY = aDecoder.decodeObject(forKey: SerializationKeys.cNY) as? Float
        self.zAR = aDecoder.decodeObject(forKey: SerializationKeys.zAR) as? Float
        self.aUD = aDecoder.decodeObject(forKey: SerializationKeys.aUD) as? Float
    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(rON, forKey: SerializationKeys.rON)
        aCoder.encode(mYR, forKey: SerializationKeys.mYR)
        aCoder.encode(iSK, forKey: SerializationKeys.iSK)
        aCoder.encode(cAD, forKey: SerializationKeys.cAD)
        aCoder.encode(dKK, forKey: SerializationKeys.dKK)
        aCoder.encode(gBP, forKey: SerializationKeys.gBP)
        aCoder.encode(pHP, forKey: SerializationKeys.pHP)
        aCoder.encode(cZK, forKey: SerializationKeys.cZK)
        aCoder.encode(pLN, forKey: SerializationKeys.pLN)
        aCoder.encode(rUB, forKey: SerializationKeys.rUB)
        aCoder.encode(sGD, forKey: SerializationKeys.sGD)
        aCoder.encode(bRL, forKey: SerializationKeys.bRL)
        aCoder.encode(jPY, forKey: SerializationKeys.jPY)
        aCoder.encode(sEK, forKey: SerializationKeys.sEK)
        aCoder.encode(uSD, forKey: SerializationKeys.uSD)
        aCoder.encode(hRK, forKey: SerializationKeys.hRK)
        aCoder.encode(nZD, forKey: SerializationKeys.nZD)
        aCoder.encode(hKD, forKey: SerializationKeys.hKD)
        aCoder.encode(bGN, forKey: SerializationKeys.bGN)
        aCoder.encode(tRY, forKey: SerializationKeys.tRY)
        aCoder.encode(mXN, forKey: SerializationKeys.mXN)
        aCoder.encode(hUF, forKey: SerializationKeys.hUF)
        aCoder.encode(kRW, forKey: SerializationKeys.kRW)
        aCoder.encode(nOK, forKey: SerializationKeys.nOK)
        aCoder.encode(iNR, forKey: SerializationKeys.iNR)
        aCoder.encode(iLS, forKey: SerializationKeys.iLS)
        aCoder.encode(iDR, forKey: SerializationKeys.iDR)
        aCoder.encode(cHF, forKey: SerializationKeys.cHF)
        aCoder.encode(tHB, forKey: SerializationKeys.tHB)
        aCoder.encode(cNY, forKey: SerializationKeys.cNY)
        aCoder.encode(zAR, forKey: SerializationKeys.zAR)
        aCoder.encode(aUD, forKey: SerializationKeys.aUD)
    }
}
