//
//  BaseListViewModel.swift
//  Conversions
//
//  Created by Daniel Griso Filho on 08/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import Foundation

class BaseListViewModel {

    // MARK: - Properties

    fileprivate var itemsViewModels = [IndexPath: BaseViewModel]()

    var numberOfItems: Int { return itemsViewModels.count }
    
    // MARK: - Initialization
    
    convenience init(_ objects: [Payload]) {
        self.init()
        self.itemsViewModels = objects.enumerated().reduce(into: [IndexPath: BaseViewModel]()) {
            let newIndexPath = IndexPath(row: $1.offset, section: 0)
            $0[newIndexPath] = BaseViewModel($1.element)
        }
    }

    // MARK: - View Model
    
    func itemViewModel(indexPath: IndexPath) -> BaseViewModel? {
        if itemsViewModels.keys.contains(indexPath) {
            return itemsViewModels[indexPath]
        }
        return nil
    }

}
