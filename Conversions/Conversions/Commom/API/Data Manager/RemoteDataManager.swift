//
//  RemoteDataManager.swift
//  Conversions
//
//  Created by Daniel Griso Filho on 07/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import Foundation

typealias RemoteCompletion = (Payload?, ResponseStatus) -> Void

class RemoteDataManager {
    
    class func getApiInformations(endPoint: String, completion: @escaping RemoteCompletion) {
        
        let newFinalURL = ServiceConstants.UrlParts.country.rawValue.replacingOccurrences(of: ServiceConstants.UrlParam.Key.country, with: endPoint)
        
        let url = URL(string: ServiceConstants.UrlParts.baseURL.rawValue + newFinalURL)
        
        guard let safeURL = url else { return }
        
        NetworkManager.request(url: safeURL, method: .get, handleObject: { (json)  in
            let payload = Payload(json: json)
            completion(payload, ResponseStatus.successStatus())
        }) { (status) in
            completion(nil, status)
        }
        
    }
}
