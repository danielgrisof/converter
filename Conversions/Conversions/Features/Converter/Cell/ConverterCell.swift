//
//  ConverterCell.swift
//  Converter
//
//  Created by Daniel Griso Filho on 05/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import UIKit
import FlagKit

class ConverterCell: UITableViewCell {
    
    // MARK: - Variables
    var viewModel: RateViewModel? { didSet { updateInfos() } }
    var newValue: Float? { didSet { updateCurrency() } }
    
    // MARK: - Outlets
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var convertedCurrencyTextField: UITextField!
    
    // MARK: - Life Cicle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Selected Action
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Internal Functions
    fileprivate func updateInfos() {
        countryLabel.text = viewModel?.name
        let shortCountryName = viewModel?.name.dropLast()
        
        let flag = Flag(countryCode: String(shortCountryName ?? ""))
        flagImageView?.image = flag?.originalImage
    }
    
    fileprivate func updateCurrency() {
        if let vm = viewModel, let value = newValue {
            convertedCurrencyTextField.text = String(format: "%.2f", vm.currency * value)
        }else {
            convertedCurrencyTextField.text = "0"
        }
    }
}
