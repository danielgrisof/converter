//
//  MockJson.swift
//  ConversionsTests
//
//  Created by Daniel Griso Filho on 10/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import Foundation

struct MockJson {
    
    static let payload: [String: Any] = [
        "base": "EUR",
        "date": "2018-09-06",
        "rates": [
            "AUD": 1.6106,
            "BGN": 1.9488,
            "BRL": 4.7747,
            "CAD": 1.5283,
            "CHF": 1.1235,
            "CNY": 7.9168,
            "CZK": 25.623,
            "DKK": 7.4301,
            "GBP": 0.89504,
            "HKD": 9.0998,
            "HRK": 7.4076,
            "HUF": 325.33,
            "IDR": 17262,
            "ILS": 4.1557,
            "INR": 83.419,
            "ISK": 127.34,
            "JPY": 129.09,
            "KRW": 1300.1,
            "MXN": 22.286,
            "MYR": 4.7948,
            "NOK": 9.7412,
            "NZD": 1.757,
            "PHP": 62.369,
            "PLN": 4.3029,
            "RON": 4.622,
            "RUB": 79.291,
            "SEK": 10.553,
            "SGD": 1.5943,
            "THB": 37.994,
            "TRY": 7.601,
            "USD": 1.1593,
            "ZAR": 17.76
            ]
        ]
    
    static let rates: [String: Any] = [
            "AUD": 1.6106,
            "BGN": 1.9488,
            "BRL": 4.7747,
            "CAD": 1.5283,
            "CHF": 1.1235,
            "CNY": 7.9168,
            "CZK": 25.623,
            "DKK": 7.4301,
            "GBP": 0.89504,
            "HKD": 9.0998,
            "HRK": 7.4076,
            "HUF": 325.33,
            "IDR": 17262,
            "ILS": 4.1557,
            "INR": 83.419,
            "ISK": 127.34,
            "JPY": 129.09,
            "KRW": 1300.1,
            "MXN": 22.286,
            "MYR": 4.7948,
            "NOK": 9.7412,
            "NZD": 1.757,
            "PHP": 62.369,
            "PLN": 4.3029,
            "RON": 4.622,
            "RUB": 79.291,
            "SEK": 10.553,
            "SGD": 1.5943,
            "THB": 37.994,
            "TRY": 7.601,
            "USD": 1.1593,
            "ZAR": 17.76
    ]
}
