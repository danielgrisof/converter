//
//  ConverterPresenterTests.swift
//  ConversionsTests
//
//  Created by Daniel Griso Filho on 10/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import XCTest
@testable import Conversions

class ConverterPresenterTests: XCTestCase {

    class converterPresenterMock: ConverterPresenterApi {
        
        var _interactor: Interactor!
        var _view: Any!
        var _router: Router!
        func setupView(data: Any) {}
        func viewHasLoaded() {}
        func viewIsAboutToAppear() {}
        func viewHasAppeared() {}
        func viewIsAboutToDisappear() {}
        func viewHasDisappeared() {}
        
        func getRates(country: String) {
            XCTAssertNotNil(country)
        }
        
        func didSuccessfullyRequest(viewModel: BaseListViewModel, rates: RateListViewModel) {
            XCTAssertNotNil(viewModel)
            XCTAssertNotNil(rates)
        }
        
        func didFailRequest(message: String) {
            XCTAssertNotNil(message)
        }
    }
    
    let presenterMock = converterPresenterMock()
    let payload = Payload(object: MockJson.payload)
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetRates() {
        presenterMock.getRates(country: "")
    }
    
    func testdidSuccessfullyRequest() {
        presenterMock.didSuccessfullyRequest(viewModel: BaseListViewModel([payload]), rates: RateListViewModel(MockJson.rates))
    }
    
    func testDidFailRequest() {
        presenterMock.didFailRequest(message: "Request fail")
    }
    
    func testInteractorReferenceFail() {
        XCTAssertNil(presenterMock._interactor)
    }
    
    func testViewReferenceFail() {
        XCTAssertNil(presenterMock._view)
    }
    
    func testRouterReferenceFail() {
        XCTAssertNil(presenterMock._router)
    }

}
