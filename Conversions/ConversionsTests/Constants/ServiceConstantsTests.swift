//
//  ServiceConstantsTests.swift
//  ConversionsTests
//
//  Created by Daniel Griso Filho on 10/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import XCTest
@testable import Conversions

class ServiceConstantsTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testUrlParts() {
        let baseUrl = ServiceConstants.UrlParts.baseURL.rawValue
        let finalUrl = ServiceConstants.UrlParts.country.rawValue
        let keyFinalUrl = ServiceConstants.UrlParam.Key.country
        
        XCTAssertEqual(baseUrl, "https://revolut.duckdns.org/latest?base=")
        XCTAssertEqual(finalUrl, "{country}")
        XCTAssertEqual(keyFinalUrl, "{country}")
        
    }

}
