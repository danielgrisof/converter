//
//  RateViewModel.swift
//  Conversions
//
//  Created by Daniel Griso Filho on 08/02/19.
//  Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import Foundation

class RateViewModel {

    // MARK: - Initialization

    convenience init(_ key: String, _ value: Any) {
        self.init()
        self.key = key
        self.value = value
    }

    // MARK: - Properties

    fileprivate var key: String?
    fileprivate var value: Any?
    
    var name: String { return key ?? "" }
    var currency: Float { return value as? Float ?? 0.0}
    
}
