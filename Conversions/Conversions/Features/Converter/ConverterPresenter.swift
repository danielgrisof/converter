//
//  ConverterPresenter.swift
//  Converter
//
//  Created by Daniel Griso Filho on 05/02/19.
//Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import Foundation

// MARK: - ConverterPresenter Class
final class ConverterPresenter: Presenter {
}

// MARK: - ConverterPresenter API
extension ConverterPresenter: ConverterPresenterApi {
    func getRates(country: String) {
        interactor.callAPI(country: country)
    }
    
    func didSuccessfullyRequest(viewModel: BaseListViewModel, rates: RateListViewModel) {
        view.updateRates(viewModel: viewModel, rates: rates)
    }
    
    func didFailRequest(message: String) {
        view.showAlert(message: message)
    }
}

// MARK: - Converter Viper Components
private extension ConverterPresenter {
    var view: ConverterViewApi {
        return _view as! ConverterViewApi
    }
    var interactor: ConverterInteractorApi {
        return _interactor as! ConverterInteractorApi
    }
    var router: ConverterRouterApi {
        return _router as! ConverterRouterApi
    }
}
