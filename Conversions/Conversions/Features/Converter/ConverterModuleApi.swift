//
//  ConverterModuleApi.swift
//  Converter
//
//  Created by Daniel Griso Filho on 05/02/19.
//Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import Foundation

//MARK: - ConverterRouter API
protocol ConverterRouterApi: RouterProtocol {
}

//MARK: - ConverterView API
protocol ConverterViewApi: UserInterfaceProtocol {
    func updateRates(viewModel: BaseListViewModel, rates: RateListViewModel)
    func showAlert(message: String)
}

//MARK: - ConverterPresenter API
protocol ConverterPresenterApi: PresenterProtocol {
    func getRates(country: String)
    func didSuccessfullyRequest(viewModel: BaseListViewModel, rates: RateListViewModel)
    func didFailRequest(message: String)
}

//MARK: - ConverterInteractor API
protocol ConverterInteractorApi: InteractorProtocol {
    func callAPI(country: String)
}
