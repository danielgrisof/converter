//
//  ConverterRouter.swift
//  Converter
//
//  Created by Daniel Griso Filho on 05/02/19.
//Copyright © 2019 Daniel Griso Filho. All rights reserved.
//

import Foundation

// MARK: - ConverterRouter class
final class ConverterRouter: Router {
}

// MARK: - ConverterRouter API
extension ConverterRouter: ConverterRouterApi {
}

// MARK: - Converter Viper Components
private extension ConverterRouter {
    var presenter: ConverterPresenterApi {
        return _presenter as! ConverterPresenterApi
    }
}
